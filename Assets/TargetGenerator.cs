using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class TargetGenerator : MonoBehaviour
{
    public GameObject targetPrefab;

    public int count = 0;
    
    void Start()
    {
        Spawn();
    }

    // Update is called once per frame
    void Update()
    {
        if (count > 10) return;
        if (Time.frameCount % 300 != 0) return;

        Spawn();
    }

    void Spawn()
    {
        var target = Instantiate(targetPrefab);
        target.transform.parent = transform;

        var pos = Random.onUnitSphere * Random.Range(30, 50);
        pos.y = -5;
        var angle = Vector3.Angle(pos, Vector3.forward);
        if (pos.x < 0) angle *= -1;

        target.transform.localPosition = pos;
        target.transform.localRotation = Quaternion.AngleAxis(angle, Vector3.up);

        target.GetComponent<TargetController>().gen = this;

        ++count;
    }
}
