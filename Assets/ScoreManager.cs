using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreManager : MonoBehaviour
{

    public GameObject text;
    public int score = 0;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        var textScore = text.GetComponent<Text>();
        // テキストの表示を入れ替える
        textScore.text = "スコア：" + score;
    }
}
