using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IgaguriController : MonoBehaviour
{
    public ScoreManager scoreManager;
    private bool isCollide = false;
    private int limit = 400;

    void Start()
    {
        var scoreObject = GameObject.Find("Score");
        scoreManager = scoreObject.GetComponent<ScoreManager>();

        scoreManager.score -= 10;
    }

    public void Shoot(Vector3 dir)
    {
        GetComponent<Rigidbody>().AddForce(dir);
    }

    void Update()
    {
        if (!isCollide) return;

        --limit;
        if (limit < 0)  Destroy(gameObject);
    }

    void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.name == "targetPrefab(Clone)")
        {
            GetComponent<ParticleSystem>().Play();
            var dist = (int)Vector3.Distance(transform.position, Camera.main.transform.position);
            scoreManager.score += 10 * dist;
        }
        GetComponent<Rigidbody>().AddForce(Vector3.zero);
        isCollide = true;
    }
}
