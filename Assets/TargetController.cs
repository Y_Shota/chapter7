using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class TargetController : MonoBehaviour
{
    public TargetGenerator gen;

    private Vector3 basePos;

    private int seed;

    // Start is called before the first frame update
    void Start()
    {
        basePos = transform.position;

        seed = Random.Range(0, 100);
    }

    // Update is called once per frame
    void Update()
    {
        var x = Math.Sin(Time.time + seed) * 15;
        var y = (Math.Cos((Time.time + seed) * 3.6) - 1) * 2;
        var translate = transform.rotation * new Vector3((float)x, (float)y, 0);
        transform.position = basePos + translate;
    }

    void OnCollisionEnter(Collision other)
    {
        Debug.Log(other.gameObject.name);
        if (other.gameObject.name == "igaguriPrefab(Clone)")
        {
            Destroy(gameObject);
            --gen.count;
        }
    }
}
