using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    private const float Speed = 0.6f;
    private const float Magnification = 2f;

    // Update is called once per frame
    void Update()
    {
        var mag = Input.GetKey(KeyCode.LeftControl) ? Magnification : 1f;
        if (Input.GetKey(KeyCode.A))
        {
            transform.Rotate(new Vector3(0, 1, 0), -Speed * mag);
        }
        else if (Input.GetKey(KeyCode.D))
        {
            transform.Rotate(new Vector3(0, 1, 0), Speed * mag);
        }
    }
}
