using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IgaguriGenerator : MonoBehaviour
{
    public GameObject igaguriPrefab;

    // Update is called once per frame
    void Update()
    {
        if (!Input.GetMouseButtonDown(0)) return;

        var igaguri = Instantiate(igaguriPrefab);

        var ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        var worldDir = ray.direction;

        igaguri.GetComponent<IgaguriController>().Shoot(worldDir.normalized * 2000);
    }
}
